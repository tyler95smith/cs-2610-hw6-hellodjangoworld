# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse

from datetime import datetime

def index(request):
    return HttpResponse("<h1>Hello, world.</h1><p>You're a swell person</p>" + str(datetime.now()))