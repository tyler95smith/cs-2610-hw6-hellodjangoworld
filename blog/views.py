# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import get_object_or_404,render
from django.views import generic
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse

from datetime import datetime

from .models import Blog, Comment

# Create your views here.
class IndexView(generic.ListView):
    template_name = 'blog/index.html'
    context_object_name = 'latest_blog_posts'
    
    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context.update({
            'comment_list': Comment.objects.all(),
        })
        return context

    def get_queryset(self):
        """Return the last five published questions."""
        return Blog.objects.order_by('-post_date')[:3]
    
class ArchiveView(generic.ListView):
    template_name = 'blog/archive.html'
    context_object_name = 'all_blog_posts'
    
    def get_queryset(self):
        return Blog.objects.order_by('-post_date')
        
class DetailView(generic.DetailView):
    model = Blog
    template_name = 'blog/detail.html'
    
def addComment(request, blog_id):
    blog = get_object_or_404(Blog, pk=blog_id)
    try:
        comment_text = request.POST['content_text']
    except (KeyError, Comment.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'blog/detail.html', {
            'error_message': "There was en error creating the new comment.",
        })
    else:
        newComment = Comment(blog = blog, comment_author = 'author', email = 'email', content_text = comment_text, post_date = datetime.today())
        newComment.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('blog:detail', args=(blog.id,)))
    
def about(request):
    return render(request, 'blog/about.html')

def tech_tips(request):
    return render(request, 'blog/tech_tips.html')