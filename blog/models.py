# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from django.utils.encoding import python_2_unicode_compatible

# Create your models here.
@python_2_unicode_compatible
class Blog(models.Model):
    title_text = models.CharField(max_length=200)
    authors_name = models.CharField(max_length=50)
    content_text = models.CharField(max_length=2000)
    post_date = models.DateTimeField('date posted')
    
    def __str__(self):
        return self.title_text

@python_2_unicode_compatible        
class Comment(models.Model):
    blog = models.ForeignKey(Blog, on_delete=models.CASCADE)
    comment_author = models.CharField(max_length=50)
    email = models.CharField(max_length=50)
    content_text = models.CharField(max_length=500)
    post_date = models.DateTimeField('date posted')
    
    def __str__(self):
        return self.content_text
    