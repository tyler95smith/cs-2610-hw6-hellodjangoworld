from django.conf.urls import url

from . import views

app_name = 'blog'
urlpatterns = [
    # ex: /blog/
    url(r'^$', views.IndexView.as_view(), name='index'),
    # ex: /polls/5/
    #url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    # ex: /polls/5/results/
    #url(r'^(?P<pk>[0-9]+)/results/$', views.ResultsView.as_view(), name='results'),
    # ex: /polls/5/vote/
    #url(r'^(?P<question_id>[0-9]+)/vote/$', views.vote, name='vote'),
    
    
    # ex: /polls/5/
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    # ex: /polls/5/vote/
    url(r'^(?P<blog_id>[0-9]+)/addComment/$', views.addComment, name='addComment'),
    # ex: /blog/archive
    url(r'^archive$', views.ArchiveView.as_view(), name="article"),
    # ex: /blog/about
    url(r'^about$', views.about, name='about'),
    # ex: /blog/tech_tips
    url(r'^tech_tips$', views.tech_tips, name='tech_tips'),
]