# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse
from django.shortcuts import render
import json
from .models import Conv

# Create your views here.
def index(request):
    return render(request, 'blog/about.html')
    
def convAPI(request):

    resp = {}
    r_to = request.GET['to']
    r_from = request.GET['from']
    r_value = int(request.GET['value'])
    
    o_to = Conv.objects.get(unit=r_to)
    o_from = Conv.objects.get(unit=r_from)

    if (r_value < 0):
    	resp['error'] = "Usage: r_value=[non-negative integer]"
    elif (r_to == ""):
        resp['error'] = "url must contain a ""to"" unit value"
    elif (r_from == ""):
        resp['error'] = "url must contain a ""from"" unit value"
    else:
        f_value = r_value * o_from.convValue
        f_value = f_value / o_to.convValue
        
    	resp = { 'units': r_to, 'value': f_value }

	return HttpResponse(json.dumps(resp))