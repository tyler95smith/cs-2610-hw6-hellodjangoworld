# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-12-05 23:49
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('unitconv', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='conv',
            old_name='value',
            new_name='convValue',
        ),
    ]
