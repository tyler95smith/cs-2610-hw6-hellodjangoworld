from django.conf.urls import url

from . import views

app_name = 'gold'
urlpatterns = [
    # ex: /unitconv/API?
    url(r'^convAPI$', views.convAPI, name='convAPI'),
    # ex: /unitconv/
    url(r'^$', views.index, name='index'),
]